<?php

namespace Modules\Datacleanse\Tests\Unit;

use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Modules\Datacleanse\Entities\DataAudit;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DataCleanseTest extends TestCase
{
    use withFaker, RefreshDatabase, DatabaseMigrations;

    protected $UserData;

    protected $user_id;

    public function setUp(): void
    {
        parent::setUp();

        $this->user_id = factory(User::class)->create()->id;

        // Generate some fake data to test against:
        $this->UserData = [
            'id' => $this->user_id,
            'Title' => $this->faker->title,
            'Name' => $this->faker->name,
            'Address' => '111 Victoria Rd, Mitcham CR4 3JQ, UK',
            'Country' => $this->faker->country,
            'DoB' => $this->faker->dateTimeThisCentury,
            'Telephone' => $this->faker->phoneNumber
        ];

    }

    /** @test */
    public function a_user_can_audit_a_users_imported_data()
    {
        //init Data Audit Model:
        $DA = New DataAudit($this->UserData);

        $this->assertEquals(0, $DA->RunAudit()['Score']);
    }

    /** @test */
    public function a_user_can_import_an_incorrect_title_and_receive_a_suggestion()
    {
        $DA = new DataAudit(['id' => $this->user_id, 'Title' => 'mr']);

        $this->assertEquals(10, $DA->RunAudit()['Score']);

        $this->assertDatabaseHas('data_audit',
            [
                'user_id' => $this->user_id,
                'type' => 'Title',
                'suggestion' => 'Mr',
                'score' => 10
            ]);
    }

    /** @test */
    public function a_user_can_import_an_incorrect_address_and_receive_a_suggestion()
    {
        $DA = new DataAudit([
            'id' => $this->user_id,
            'Address' => '111 Victria Road, Lodon, CR4 3JD' // Actual: 111 Victoria Road, London, CR4 3JD
        ]);

        $this->assertEquals(100, $DA->RunAudit()['Score']);

        $this->assertDatabaseHas('data_audit',
            [
                'user_id' => $this->user_id,
                'type' => 'Address',
                'suggestion' => '111 Victoria Rd, Mitcham CR4 3JQ, UK',
                'score' => 100
            ]);
    }
}
