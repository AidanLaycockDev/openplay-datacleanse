<?php

namespace Modules\Datacleanse\Entities;

use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Symfony\Component\VarDumper\Cloner\Data;

class DataAudit extends Model
{
    protected $table = 'data_audit';

    protected $fillable = ['user_id', 'type', 'suggestion', 'score'];

    protected $data = [];
    protected $data_options = [];

    protected $score = 0;

    public function __construct($data = null)
    {
        $this->data = $data;
        // Get and assign Data_Options
        $this->data_options();
    }

    /*
     *    Audit Function:
     *
     *  - Added Scores to DB
     *  - Returns Score and advice for user to be compiled against data
     *    and then displayed on page
     *
     */

    public function RunAudit()
    {
        // Run Audit against all data:
        foreach($this->data as $key => $value)
        {
            switch($key){
                case 'Title':
                    $this->Title_audit($value);
                    break;
                case 'Name':
                    $this->Name_audit($value);
                    break;
                case 'Address':
                    $this->Address_audit($value);
                    break;
                case 'Country':
                    $this->Country_audit($value);
                    break;
                case 'DoB':
                    $this->DoB_audit($value);
                    break;
                case 'Telephone':
                    $this->Telephone_audit($value);
                    break;
            }
        }
        // Return data to user:
        return ['Score' => $this->score];
    }

    /*
     * Set Options for Data Audit Process
     *
     * Score to be factored in for each business use case on how important
     * it would be for that user to have the column to be 100% correct.
     *
     // TODO: Refactor to pull this information from DB going forward or into a config file pending on final use case
     */
    public function data_options()
    {
        $this->data_options = [
            'Title' => ['Enabled' => true, 'Score' => 10],
            'Name' => ['Enabled' => true, 'Score' => 30],
            'Address' => ['Enabled' => true, 'Score' => 100],
            'Country' => ['Enabled' => true, 'Score' => 10],
            'DoB' => ['Enabled' => true, 'Score' => 10],
            'Telephone' => ['Enabled' => true, 'Score' => 10],
        ];
    }

    // Data Validation: //

    /*
     * Expects String Input
     * Returns Bool and add incorrect data & suggestions to DB
     */
    public function Title_audit($title)
    {
        if( ! preg_match('/^\p{Lu}/mu', $title))
        {
            // Generate Suggestion for correct value:
            $suggest = ucfirst($title);

            // Increment the Overall Data Score relative to Options weighting:
            $this->score += $this->data_options['Title']['Score'];

            // Save DB in DB:
            $this->fill([
                'user_id' => $this->data['id'],
                'type' => 'Title',
                'suggestion' => $suggest,
                'score' => $this->data_options['Title']['Score']
            ])->save();

            return false;
        } else {
            return true;
        }
    }

    public function Name_audit($Name)
    {

    }

    public function Address_audit($address)
    {
        if(env('Google_Maps_API')) {
            // Maps API is present, generate URL:
            $url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' . $address . '&key=' . env('Google_Maps_API');
            $c = New Client();
            $response = $c->get($url);

            $google_data = json_decode($response->getBody()->getContents(), $url);

            // is the searched for Data a Partial Match:
            if($google_data['results'][0]['formatted_address'] != $address)
            {
                // Add Google Formatted Address to Suggested:
                $suggest = $google_data['results'][0]['formatted_address'];

                $this->score += $this->data_options['Address']['Score'];

                // Save DB in DB:
                $this->fill([
                    'user_id' => $this->data['id'],
                    'type' => 'Address',
                    'suggestion' => $suggest,
                    'score' => $this->data_options['Address']['Score']
                ])->save();

                return false;
            }
            return true;
        }
    }

    public function Country_audit($country)
    {

    }

    public function DoB_audit($DoB)
    {

    }

    public function Telephone_audit($telephone)
    {

    }
}
