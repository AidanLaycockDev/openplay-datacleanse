# Open Play Data Cleanse Project

## Project Overview:
To create a data auditing application that works through imported user data to assess how 
'dirty' the data is so that the user can be notified the next time they login.

## Stack:
This project uses Laravel (https://laravel.com/) to power the application.
The DB is currently powered by MySQL for production and SQLite/Array for Testing.

## Installation:
To install the project, pull the repo onto a server/local machine, use composer to install
all dependencies (Composer install).

## Testing:
To run test suite, run /vendor/bin/phpunit from terminal.

## Items to note:
This projects uses Laravel Modules to separate concerns for separate business logic.
(https://nwidart.com/laravel-modules/v6/introduction).

You will also need a google Maps API Key to utilise the Address Validation process.
(https://developers.google.com/places/web-service/get-api-key). 

## Next steps:
The next steps for this would be:
- Adding in the rest of the Validation Rules,
- Adding in an artisan command to enable an automated Cron Job to be run against user data
on a regular basis.
